
let etatSlider = 1;



let imageLocalStorage = () => {


        // Recuperation du local storage

        let image1 = localStorage.getItem('image1');
        let image2 = localStorage.getItem('image2');
        let image3 = localStorage.getItem('image3');

        // Mise a jour de l'affichage

        document.getElementById('slider-maison').innerHTML = `
        <div id="image-slider-1" class="transition-slider"><img src="${image1}" alt="Caméra"></div>
        <div id="image-slider-2" class="transition-slider"><img src="${image2}" alt="Photographie"></div>
        <div id="image-slider-3" class="transition-slider"><img src="${image3}" alt="Photographie"></div>
        `


};

imageLocalStorage();

// Fonction qui change l'image
let changementImage = () => {

    // Masquer l'image en cours
    let recuperationImage = document.getElementById(`image-slider-${etatSlider}`);
    recuperationImage.style.display = "none";

    // Afficher la nouvelle image
    if (etatSlider < 3) {
        etatSlider++;
    } else {
        etatSlider = 1;
    }
    recuperationImage = document.getElementById(`image-slider-${etatSlider}`);
    recuperationImage.style.display = "block";

};

// Toutes les 5 secondes
window.setInterval(changementImage, 5000);
