// Remplissage du formulaire par défaut
let remplissage = () => {

    // Recuperation du local storage
    let image1 = localStorage.getItem('image1');
    let image2 = localStorage.getItem('image2');
    let image3 = localStorage.getItem('image3');

    // Mise à jour
    document.getElementById('image1').value = image1;
    document.getElementById('image2').value = image2;
    document.getElementById('image3').value = image3;

};

remplissage();

let majStorage = () => {

    let image1 = document.getElementById('image1').value;
    let image2 = document.getElementById('image2').value;
    let image3 = document.getElementById('image3').value;

    localStorage.setItem('image1', image1);
    localStorage.setItem('image2', image2);
    localStorage.setItem('image3', image3);

};
